wget http://registrationcenter-download.intel.com/akdlm/irc_nas/15512/l_openvino_toolkit_p_2019.1.144.tgz
tar -xvzf l_openvino_toolkit_p_2019.1.144.tgz
cd l_openvino_toolkit_p_2019.1.144
sudo ./install_openvino_dependencies.sh
sudo ./install.sh
cd /opt/intel/openvino/install_dependencies
sudo -E ./install_openvino_dependencies.sh
source /opt/intel/openvino/bin/setupvars.sh
cd /opt/intel/openvino/deployment_tools/model_optimizer/install_prerequisites
sudo ./install_prerequisites.sh
cd /opt/intel/openvino/deployment_tools/demo
./demo_squeezenet_download_convert_run.sh
./demo_security_barrier_camera.sh
sudo apt-get install libgflags-dev
cd /opt/intel/openvino_2019.1.144/deployment_tools/inference_engine/samples/
sudo ./build_samples.sh
echo "export InferenceEngine_DIR=~/inference_engine_samples_build/" >> ~/.bashrc
echo "export CPU_EXTENSION_LIB=~/inference_engine_samples_build/intel64/Release/lib/libcpu_extension.so" >> ~/.bashrc
echo "export GFLAGS_LIB=~/inference_engine_samples_build/intel64/Release/lib/libgflags_nothreads.a" >> ~/.bashrc
echo "source /opt/intel/openvino/bin/setupvars.sh" >> ~/.bashrc
